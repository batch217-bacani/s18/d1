// console.log("Hello World");

// [SECTION] Functions

// Parameters and Arguments

// function printInput(){
// 	let nickname = prompt("Enter your nickname:");
// 	console.log("Hi, " + nickname);
// }

// printInput();

// This function has parameter and argument
function printName(name){ //name --> is our parameter
	console.log("My name is " + name)
}

printName("Gelo"); // "Juana" is our --> Argument

// 1. printName("Juana"); >> Argument
// 2. will be stored to our PARAMETER >> function printName(name)
// 3. The information/data stored in a Parameter can be used to the code block inside a function.

let sampleVariable = "Yui"

printName(sampleVariable);

// Function arguments cannot be used by a function if there are no parameter provided within the function.

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(32);
checkDivisibilityBy8(69);

// You can also do the same using prompt(), however, take not that prompt() output a string, Strings are not ideal for mathematical computations.

// Function as Arguments

// Function parameters can also accept other functions as arguments
// some complex functions use other functions as arguments to perform more complicated results
// This will be further seen when we discuss array method.

function argumentFunction(){
	console.log("This function was passed as an argument before the message.")
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

// Adding and removing the parentheses "()" impacts the output of JavaScript heavily
// When a function is used with parentheses "()", it denotes invoking/calling a function
// A function used without a parenthesis is normally associated with using the function as an argument to another function

invokeFunction(argumentFunction);

// or finding more information about a function in the console using console.log();

console.log(argumentFunction);

// Multiple Parameters

function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName("Angelo", "Salas", "Bacani");

// Multiple parameter using stored data in a variable
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

function printFullName(middleName, firstName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

printFullName("Juan", "Dela", "Cruz");

// [SECTION] Return statement
// The "retur" statement allow us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This will not be listed in the console kasi nsa ilalim siya ni return promise");
}

let completeName = returnFullName("Juan", "Dela", "Cruz");
console.log(completeName);

// in our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.

// whatever value is returned from the "returnFullName" function is stored in the "completeName" variable

console.log(returnFullName(firstName, middleName, lastName));

// You can also create a variable inside the function to contain the result and return that variable instead.

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
	console.log("This will not be displayed bitch");
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// on the other hand, when a function that only has console.log() to display its result it will return undefined instead.

function printPlayerInfo(username, level, job){
	// console.log("Username: " + username);
	// console.log("Level: " + level);
	// console.log("Job: " + job);

	return "Username " + username;
}

let user1 = printPlayerInfo("Knight_White", 95, "Paladin");
console.log(user1);

// returns undefined because printPlayerInfo returns nothing. It only console. logs the details.

// You cannot save any value from printPlayerInfo() because it does not return anything.